#Important points
1. Aclass is an encapsulation of data and behaviour
2. Data Should be assigned to a variable
3. Variable should follow the naming rules and conventions
   Naming Rules:
  ->Should not be a keyword/reserve word
  ->can contain only alphanumeric letters
  ->_ and $vare the only two special characters allowed
  ->Cannot start with a number
  ->Variables are case sensitive

  Conventions:
  ->method name and Variables should start with lower case
  ->class,interface,Enum should start with Upper case letters
  ->CamelCase Convention should be followed

4. Both data and methods together are called instance members of the class
5. Instance variables are intialized with the default values
6. Class is a template through which we can create objects
7. Instance variables are unique to each object created from the class
8. Objects are created by invoking the new operator on the constructor
9. Constructor should ahve the same name as the class namewithout any return type
10. Objects created are stored in the heap memory and can be assigned a reference (also called as handle)
11. Using the handle we can call the methods on the object
12. All classes directly or indirectly extends the Object class
13. The constructor can be overloaded
14. Overloading happens in the same class.Overloading can be either constructor or method overloading
15. Overloading- Same method name but different number of arguments/different types of arguments
16. Overloading is a type of compile time polymorphism
17. The constructor calling resultions happens at compile time based on the number/type of arguments
18. Methods which do not have a return value should be declared as void return type.
19. If a method returns a value, then the data type should be declared in the method signature
20. Members that are not specific to objects and are common to all the objects are called static members
21. Static methods are class level and shared by all the objects
22. Static methods cannot access non-static members directly,have to access them via the objects
23. Static methods cannot use this reference,as this refrence points to the current object
24. Non static methods also called as instance methods,can access static members
25. Non static methods can access the this keyword which points to the current objects at hand.
26. To defined fixed values, we define Enums
27. Enums are used to define constants,convention is to use the uppercase letters
28. Classes/interfaces/enums are organized into pacakges
29. package statement should be the first to be added in the class definition
30. The import statements follow the pacakge declaration
31. There are four access modifiers (private, default, protected and public)
32. Private members are specfic to class and not even shared by the child class
33. Private members are not visible outside of the current class.
34. private keyword can be applied to variables, methods and constructors 
35. variables declared inside the method cannot have the access modifiers
36. Variables declared inside the method should be initialized before they can be accessed 
37. Variables declared inside the method will not be initialized with a defualt value.
38. Arrays are contigouls block of memory
39. Arrays size is fixed
40. Arrays have only the length property 
41. Arrays hold similar data type objects and primitives
42. Elements inside the arrays can be accessed using the index
43. If you try to access the elements in the index more than the size, you will get IndexOutOfBoundsException
44. default members have the visibility only within the package
45. The default keyword can be applied to class, variables, methods and constructors
46. protected variables can be accessed by all the members within the package just like default but can also be accessed by members outside of the package but only through the subclass.
47. protected access modifier can be applied to methods, variables and constructor
48. public members can be accessed everywhere (within package and ouside the package).
49. public modifier can be applied to class,constructor, method and variables.
50. A class can extend only one class
51. All classes by default access the Object class directly or indirectly.
52. Class can extend another class using the `extends` keyword.
53. All the members except the private members, static members and instnce ariable and static variables are inherited to the child class.
54. The sub class can choose to override the implementation by redifining the method definition
55. Method overriding resolution happens at runtime
56. Methods that override should have the same signature and return type(or sub class of the return type) 
57. Parent class reference can be assigned to the child class object
58. Using the parent class reference we can only call the members defined in the parent class
59. When calling the method using the reference, the method resolution will happen during runtime.
60. A parent class can choose not to implement methods in which case it should be declared `abstact`
61. Even if one of the method is ceclared as abstract, the whole class should be declared as abstact.
62. An abstract class can have both abstract and non abstract methods
63. An abstract cass can have a constuctor.
64. An abstract class cannot be instantiated and can be instantiated by calling the constructor of its subclass which must be concrete.
65. A concrete class should implement all the abstract methods else it should be declared as abstract
66. `final` keyword can be applied to ensure that no changes can be done
67. `final` keyword can be applied to a variable, method and class.
    1.  variable cannot be reassigned with a value
    2.  method cannot be overridden
    3.  class cannot be extended

68. Methods cannot be abstract and final at the same time.
69. All collections classes and interfaces reside in the `java.util` package
70. There are 4 main family of collections
    1.  List - Index based and allow duplicates
    2.  Set  - Does not allow duplicated
    3.  Queue - maintains FIFO order
    4.  Map - supports key -> value pairs

71. List is an interface
72. Concrete implementations are `ArrayList` and `LinkedList`
73. Concrete implmentations of Set interface are `HashSet`, `LinkedHashSet` and `TreeSet`
74. Concrete implementatios of Queue interface is `PriorityQueue`
75. Concrete implementations of Map interface are `HashMap`, `LinkedHashMap` and `TreeMap`
